﻿using DownloadFilesManager.Integration.Tests.Setup;
using System.Threading.Tasks;
using Xunit;
using Shouldly;
using System.Collections.Generic;

namespace DownloadFilesManager.Integration.Tests.Features
{
    [Collection("Sequential")]
    public class DownloadConsoleFeature : IClassFixture<CommonFixture>,IClassFixture<SetupHttpHostFixture>, IClassFixture<SetupFtpHostFixture>
    {
        private readonly CommonFixture commonFixture;        

        private const string urlHash = "d15861c7fe976303e93916013c3dedbfd86f124f72e77fdbe0f838451a233304";
        private const string ftpUrlHash = "2ad8ad29729b9a69a2865ae67b15ad687c7066eb9554571901347617518e034e";

        private readonly List<string> expectedFiles = new List<string> {
            string.Concat(urlHash,"-test.txt"),
            string.Concat(urlHash,"-subfolder1-subfolder1-1-test.txt"),
            string.Concat(urlHash,"-subfolder1-test.txt"),
            string.Concat(urlHash,"-subfolder2-subfolder2-1-test.txt"),
            string.Concat(urlHash,"-subfolder2-test.txt"),
            string.Concat(urlHash,"-httpfolder-test.txt"),
            string.Concat(ftpUrlHash,"-ftpTest.txt"),
            string.Concat(ftpUrlHash,"-subfolder1-ftpTest.txt"),
            string.Concat(ftpUrlHash,"-subfolder1-subsubfolder1-1-ftpTest.txt"),
            string.Concat(ftpUrlHash,"-subfolder2-ftpTest.txt"),
            string.Concat(ftpUrlHash,"-subfolder2-subfolder2-1-ftpTest.txt"),
        };

        private string HttpHost { get; set; }
        private string FtpHost { get; set; }

        public DownloadConsoleFeature(SetupHttpHostFixture setupHttpFixture,CommonFixture commonFixture, SetupFtpHostFixture setupFtpHostFixture)
        {                        
            HttpHost = setupHttpFixture.HostAddress;
            FtpHost = setupFtpHostFixture.FtpHost;
            this.commonFixture = commonFixture;
        }

        [Fact]
        public async Task DownloadFilesFromConsoleApp()
        {
            try
            {
                await Program.Main(new string[]
                    { 
                      $"-p {HttpHost},{FtpHost}"
                    });
                
                expectedFiles.ForEach(item => commonFixture.GetExpectedFile(commonFixture.TestDropFolder, item).ShouldNotBeNull());
            }
            finally
            {
                commonFixture.ClearDropFolder(commonFixture.TestDropFolder);                
            }
        }       
    }
}
