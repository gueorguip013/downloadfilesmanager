﻿using DownloadFilesManager.Integration.Tests.Setup;
using System.Threading.Tasks;
using Xunit;
using Shouldly;
using System.Collections.Generic;
using DownloadFilesManager.Services;
using DownloadFilesManager.Services.Factories.ProtocolFactory;
using DownloadFilesManager.Services.Configurations;
using Microsoft.Extensions.DependencyInjection;

namespace DownloadFilesManager.Integration.Tests.Features
{
    [Collection("Sequential")]
    public class DownloadLibraryFeature : IClassFixture<CommonFixture>,IClassFixture<SetupHttpHostFixture>, IClassFixture<SetupFtpHostFixture>
    {
        private readonly CommonFixture commonFixture;        

        private const string urlHash = "d15861c7fe976303e93916013c3dedbfd86f124f72e77fdbe0f838451a233304";
        private const string ftpUrlHash = "2ad8ad29729b9a69a2865ae67b15ad687c7066eb9554571901347617518e034e";
        private string HttpHost { get; set; }
        private string FtpHost { get; set; }

        private readonly List<string> expectedFiles = new List<string> {
            string.Concat(urlHash,"-test.txt"),
            string.Concat(urlHash,"-subfolder1-subfolder1-1-test.txt"),
            string.Concat(urlHash,"-subfolder1-test.txt"),
            string.Concat(urlHash,"-subfolder2-subfolder2-1-test.txt"),
            string.Concat(urlHash,"-subfolder2-test.txt"),
            string.Concat(urlHash,"-httpfolder-test.txt"),
            string.Concat(ftpUrlHash,"-ftpTest.txt"),
            string.Concat(ftpUrlHash,"-subfolder1-ftpTest.txt"),
            string.Concat(ftpUrlHash,"-subfolder1-subsubfolder1-1-ftpTest.txt"),
            string.Concat(ftpUrlHash,"-subfolder2-ftpTest.txt"),
            string.Concat(ftpUrlHash,"-subfolder2-subfolder2-1-ftpTest.txt"),
        };
        
        public DownloadLibraryFeature(SetupHttpHostFixture httpFixture,CommonFixture commonFixture, SetupFtpHostFixture setupFtpHostFixture)
        {                        
            HttpHost = httpFixture.HostAddress;
            FtpHost = setupFtpHostFixture.FtpHost;
            this.commonFixture = commonFixture;
        }
        
        [Fact]
        public async Task DownloadFilesFromLibrary()
        {            
            try
            {                
                DependencyConfiguration.Configure();
                var downloadManager = new DownloadFileManager(new ProtocolFactory(DependencyConfiguration.Container), 
                    DependencyConfiguration.Container.GetService<IFilesConfiguration>());

                var filesNumber = await downloadManager.DownloadFiles(new string[] { HttpHost, FtpHost });

                filesNumber.ShouldBe(11);
                expectedFiles.ForEach(item => commonFixture.GetExpectedFile(commonFixture.TestDropFolder, item).ShouldNotBeNull());
                
            }
            finally
            {
                commonFixture.ClearDropFolder(commonFixture.TestDropFolder);                
            }
        }

    }
}
