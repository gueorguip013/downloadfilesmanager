﻿using System.IO;
using System.Linq;

namespace DownloadFilesManager.Integration.Tests.Setup
{
    public class CommonFixture
    {
        public string TestDropFolder { get; } = @"C:\\DownloadFilesManager.Integration.Tests.TempFiles";
        public string GetExpectedFile(string dropFolder,string expectedFileName) {
            var paths = Directory.GetFiles(dropFolder, "*", SearchOption.TopDirectoryOnly);
            return paths.FirstOrDefault(s => s.Contains(expectedFileName));
        }

        public void DeleteFile(string dropFolder, string path)
        {
            var fullPath = Path.Combine(dropFolder, path);
            if(File.Exists(fullPath)) File.Delete(fullPath);
        }
        public void ClearDropFolder(string dropFolder) {
            if(Directory.Exists(dropFolder)) Directory.Delete(dropFolder, true);
        }
    }
}
