﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using System.IO;

namespace DownloadFilesManager.Integration.Tests.Setup
{
    public class HttpStartupTest
    {
        public void ConfigureServices()
        {

        }
        
        public void Configure(IApplicationBuilder app)
        {
            var mockFolder = @"MockFiles/wwwroot";

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), mockFolder))
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), mockFolder))
            });
        }

    }
}
