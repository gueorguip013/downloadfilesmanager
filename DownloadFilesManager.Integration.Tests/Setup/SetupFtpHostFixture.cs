﻿using Zhaobang.FtpServer;
using System.Threading;
using System.Net;
using System.IO;

namespace DownloadFilesManager.Integration.Tests.Setup
{
    public class SetupFtpHostFixture
    {
        private FtpServer ftpServer;
        private readonly string ftpHostFolder = Path.Combine(Directory.GetCurrentDirectory(), "MockFiles\\ftpfolder");
        private readonly CancellationTokenSource cancelSource;
        private const string FtpIpAdress = "127.0.0.2";
        public string FtpHost { get; } = $"ftp://{FtpIpAdress}";
        public  SetupFtpHostFixture()
        {
            cancelSource = new CancellationTokenSource();
            CreateFtpServer();            
        }

        private void EnsureFtpHostFolder() 
        {            
            if (!Directory.Exists(ftpHostFolder)) Directory.CreateDirectory(ftpHostFolder);
        }

        private void CreateFtpServer() 
        {
            EnsureFtpHostFolder();

            var endPoint = new IPEndPoint(IPAddress.Parse(FtpIpAdress), 21);
            var baseDirectory = ftpHostFolder;
            
            ftpServer = new FtpServer(endPoint, baseDirectory);
            ftpServer.RunAsync(cancelSource.Token);
        }

    }

}
