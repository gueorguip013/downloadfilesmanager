﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;

namespace DownloadFilesManager.Integration.Tests.Setup
{
    public class SetupHttpHostFixture : IDisposable
    {
        private const string RootAddress = "http://localhost:5050";
        public string HostAddress { get; } = $"{RootAddress}/httpfolder";
        private readonly IHost host;
        private bool disposed = false;
        public SetupHttpHostFixture()
        {
            host = CreateHostBuilder().Build();
            host.Start();
        }
        public IHostBuilder CreateHostBuilder() =>
            Host.CreateDefaultBuilder()                
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<HttpStartupTest>()
                    .UseUrls(RootAddress)
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseKestrel();                                    
                });

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected async virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                await host.StopAsync();
            }

            disposed = true;
        }


    }
}
