﻿namespace DownloadFilesManager.Services.Configurations
{
    public class FilesConfiguration : IFilesConfiguration
    {
        public string FileNamesRegEx { get; set; } = "<a href=(?!\"/\")(?!\"../\")(?!\"./\")\"(?<file>.*?)\"";
        public int MaxDowloadFilesConcurrent { get; set; } = 5;
        public string DropFolder { get; set; } = "C:\\DownloadFilesManagerDefaultPath";
    }
}
