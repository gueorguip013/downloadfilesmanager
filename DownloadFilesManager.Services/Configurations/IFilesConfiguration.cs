﻿namespace DownloadFilesManager.Services.Configurations
{
    public interface IFilesConfiguration
    {
        int MaxDowloadFilesConcurrent { get; set; }
        string FileNamesRegEx { get; set; }
        string DropFolder { get; set; }
    }
}
