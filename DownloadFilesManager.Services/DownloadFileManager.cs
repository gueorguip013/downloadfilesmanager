﻿using DownloadFilesManager.Services.Configurations;
using DownloadFilesManager.Services.Factories.ProtocolFactory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using DownloadFilesManager.Services.Helpers;

[assembly: InternalsVisibleTo("DownloadFilesManager.Unit.Tests")]
namespace DownloadFilesManager.Services
{
    public class DownloadFileManager: IDownloadFileManager
    {
        private readonly IProtocolFactory protocolFactory;
        private readonly IFilesConfiguration config;
        public DownloadFileManager(IProtocolFactory protocolFactory, IFilesConfiguration config)
        {
            this.protocolFactory = protocolFactory;
            this.config = config;
        }
        
        public async Task<int> DownloadFiles(IEnumerable<string> sourcePaths, IProgress<string> progress = null, CancellationToken cancellationToken = default) 
        {
            try
            {
                var totalFiles = 0;

                foreach (var path in sourcePaths.Where(s => s.Length > 0))
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    var url = new Uri(path);
                    var protocol = protocolFactory.CreateObject(url.Scheme);
                    if (protocol == null)
                        throw new Exception("Protocol or path not supported.");

                    totalFiles += await protocol.DownloadFilesAsync(url, progress, cancellationToken);
                }

                return totalFiles;
            }            
            catch
            {                
                FilesHelper.DeleteFolder(config.DropFolder);                
                throw;
            }
        }
    }
}
