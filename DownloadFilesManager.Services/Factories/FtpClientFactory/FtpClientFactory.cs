﻿using FluentFTP;
using System.Net;

namespace DownloadFilesManager.Services.Factories.FtpClientFactory
{    
    public class FtpClientFactory : IFtpClientFactory
    {        
        public IFtpClient CreateFtpClient(string url, int port = 21, NetworkCredential credentials = null)
        {                        
            var ftpClient = new FtpClient
            {
                Host = url,
                Port = port
            };

            if (credentials != null) ftpClient.Credentials = credentials;

            ftpClient.Connect();

            return ftpClient;
        }

    }
}
