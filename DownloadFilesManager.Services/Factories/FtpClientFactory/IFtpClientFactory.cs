﻿using FluentFTP;
using System.Net;

namespace DownloadFilesManager.Services.Factories.FtpClientFactory
{
    public interface IFtpClientFactory
    {
        IFtpClient CreateFtpClient(string url, int port = 21, NetworkCredential credentials = null);
    }
}
