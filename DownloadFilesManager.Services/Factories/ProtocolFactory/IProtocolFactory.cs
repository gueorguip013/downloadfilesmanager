﻿using DownloadFilesManager.Services.Protocols;

namespace DownloadFilesManager.Services.Factories.ProtocolFactory
{
    public interface IProtocolFactory
    {
        IProtocol CreateObject(string protocolName);
    }
}
