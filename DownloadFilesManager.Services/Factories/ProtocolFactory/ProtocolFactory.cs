﻿using DownloadFilesManager.Services.Protocols;
using DownloadFilesManager.Services.Protocols.Ftp;
using DownloadFilesManager.Services.Protocols.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace DownloadFilesManager.Services.Factories.ProtocolFactory
{
    public class ProtocolFactory : IProtocolFactory
    {        
        private readonly Dictionary<string, Func<IProtocol>> protocolFactory;
        
        public ProtocolFactory(ServiceProvider container)
        {                 
            protocolFactory = new Dictionary<string, Func<IProtocol>>
            {
                { Uri.UriSchemeFtp, () => container.GetService<IFtpProtocol>() },
                { Uri.UriSchemeHttp, () => container.GetService<IHtmlProtocol>() },
            };
        }

        public IProtocol CreateObject(string protocolName)
        {
            var factory = protocolFactory.TryGetValue(protocolName, out var protocolObject) ? protocolObject : null;
            return factory == null ? null : factory();
        }
    }
}
