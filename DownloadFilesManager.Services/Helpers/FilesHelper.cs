﻿using System.IO;

namespace DownloadFilesManager.Services.Helpers
{
    public static class FilesHelper
    {
        public static void DeleteFolder(string folderPath)
        {            
            if (Directory.Exists(folderPath)) Directory.Delete(folderPath, true);            
        }

        public static void CreateFolderIfNotExists(string folderPath) {
            if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);
        }
    }
}
