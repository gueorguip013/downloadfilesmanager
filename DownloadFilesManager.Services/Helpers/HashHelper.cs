﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace DownloadFilesManager.Services.Helpers
{
    internal static class HashHelper
    {
        public static string GetHash(string rootPath)
        {
            if (string.IsNullOrEmpty(rootPath))
                return string.Empty;

            using (var sha = new SHA256Managed())
            {
                byte[] textData = Encoding.UTF8.GetBytes(rootPath);
                byte[] hash = sha.ComputeHash(textData);
                return BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
            }
        }
    }
}
