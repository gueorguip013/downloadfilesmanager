﻿using System.IO;

namespace DownloadFilesManager.Services.Helpers
{
    public class PathInfo
    {
        private string ReplaceSlash(string path) => path.TrimStart(Path.AltDirectorySeparatorChar).Replace(Path.AltDirectorySeparatorChar.ToString(), "-");
        private string GetFilePath(string path) => string.IsNullOrEmpty(path) ? "-" : $"-{ReplaceSlash(path.TrimEnd(Path.AltDirectorySeparatorChar))}-";
        private string GetSegment(string segment) {

            var trimSegment = segment.Trim(Path.AltDirectorySeparatorChar);

            return string.IsNullOrEmpty(trimSegment) ?
                        string.Empty :
                        $"{trimSegment}{Path.AltDirectorySeparatorChar}";

        } 

        public string Name { get; set; }
        public string FoldersPath { get; set; }        
        public bool IsDirectory { get; set; }
        public string GetDropFileName() => $"{GetFilePath(GetSegment(FoldersPath))}{Name}";
        public string AbsoluteFilePath(string rootPath) => $"{GetSegment(rootPath)}{GetSegment(FoldersPath)}{Name}";
        public string RelativeFilePath() => $"{GetSegment(FoldersPath)}{Name}";
    }
}
