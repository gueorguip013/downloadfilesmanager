﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DownloadFilesManager.Services
{
    public interface IDownloadFileManager
    {
        Task<int> DownloadFiles(IEnumerable<string> sourcePaths, IProgress<string> progress = null, CancellationToken cancellationToken = default);
    }
}
