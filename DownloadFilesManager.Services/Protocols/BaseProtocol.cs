﻿using DownloadFilesManager.Services.Configurations;
using DownloadFilesManager.Services.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Linq;

namespace DownloadFilesManager.Services.Protocols
{
    public abstract class BaseProtocol
    {
        protected IFilesConfiguration Config { get; }
        private string rootUrl;
        private readonly ConcurrentDictionary<string, string> downloadedFiles;
        private bool IsPathProcessed(string path) => downloadedFiles.TryGetValue(path, out _);
        private void AddPathToProcessedList(string path) => downloadedFiles.TryAdd(path, null);
        protected string RootPath
        {
            get => rootUrl;
            set => rootUrl = !value.EndsWith(Path.AltDirectorySeparatorChar)
                    ? string.Concat(value, Path.AltDirectorySeparatorChar)
                    : rootUrl = value;
        }

        protected BaseProtocol(IFilesConfiguration config)
        {
            Config = config;
            downloadedFiles = new ConcurrentDictionary<string, string>();
        }

        protected string GetDropFullFilePath(PathInfo pathInfo) => $"{Config.DropFolder}{Path.DirectorySeparatorChar}{HashHelper.GetHash(RootPath)}{pathInfo.GetDropFileName()}";
        protected abstract Task<IEnumerable<PathInfo>> GetFilesFromSourceAsync(Uri url, CancellationToken cancellationToken = default);
        protected abstract Task DownloadAndSaveFilesAsync(PathInfo pathInfo, CancellationToken cancellationToken = default);
        protected void EnsureDropFolderExists() => FilesHelper.CreateFolderIfNotExists(Config.DropFolder);        
        public Task<int> DownloadFilesAsync(Uri url, IProgress<string> progress = null, CancellationToken cancellationToken = default)
        {
            RootPath = url.ToString();
            return GetAndDownloadFilesAsync(new Uri(RootPath), progress, cancellationToken);
        }

        private void EnqueuePaths(ConcurrentQueue<PathInfo> queue, IEnumerable<PathInfo> pathList)
        {
            foreach (var item in pathList)
            {
                var fullPath = item.AbsoluteFilePath(RootPath);
                if (IsPathProcessed(fullPath)) continue;

                AddPathToProcessedList(fullPath);

                queue.Enqueue(item);
            }
        }

        private void SendProgress(IProgress<string> progress, string value) {
            if (progress != null) progress.Report(value);
        }
        
        private async Task<int> GetAndDownloadFilesAsync(Uri url, IProgress<string> progress, CancellationToken cancellationToken = default)
        {        
            var taskQueue = new ConcurrentQueue<PathInfo>();
            var runningList = new ConcurrentBag<Task>();
            var processingFolderCount = 0;
            var fileCounter = 0;            
            EnqueuePaths(taskQueue, await GetFilesFromSourceAsync(url,cancellationToken));

            using (var throttler = new SemaphoreSlim(Config.MaxDowloadFilesConcurrent))
            {
                while (!taskQueue.IsEmpty || processingFolderCount > 0)
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    if (taskQueue.TryDequeue(out var pathInfo))
                    {
                        await throttler.WaitAsync();

                        if (pathInfo.IsDirectory)
                        {
                            Interlocked.Increment(ref processingFolderCount);
                            runningList.Add(Task.Run(async () =>
                            {
                                try
                                {
                                    EnqueuePaths(taskQueue, await GetFilesFromSourceAsync(new Uri(pathInfo.AbsoluteFilePath(RootPath)), cancellationToken));
                                }
                                finally
                                {
                                    Interlocked.Decrement(ref processingFolderCount);
                                    throttler.Release();
                                }
                            }));
                        }
                        else
                        {
                            runningList.Add(Task.Run(async () =>
                            {
                                try
                                {
                                    SendProgress(progress, $"\nStarting dowloading {pathInfo.AbsoluteFilePath(RootPath)}...");
                                    await DownloadAndSaveFilesAsync(pathInfo, cancellationToken);
                                    SendProgress(progress, $"\nDone {Path.GetFileName(GetDropFullFilePath(pathInfo))}");
                                }
                                finally
                                {
                                    throttler.Release();
                                }
                            }));

                            Interlocked.Increment(ref fileCounter);
                        }
                    }

                    if (runningList.Any(t => t.Status == TaskStatus.Faulted)) break;
                }

                await Task.WhenAll(runningList);
            }
            
            return fileCounter;
        }
    }
}
