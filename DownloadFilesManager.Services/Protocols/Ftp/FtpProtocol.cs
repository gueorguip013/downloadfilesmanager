﻿using DownloadFilesManager.Services.Configurations;
using DownloadFilesManager.Services.Factories.FtpClientFactory;
using DownloadFilesManager.Services.Helpers;
using FluentFTP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace DownloadFilesManager.Services.Protocols.Ftp
{
    public class FtpProtocol : BaseProtocol, IFtpProtocol
    {
        private readonly IFtpClientFactory ftpClientFactory;
        public FtpProtocol(IFilesConfiguration config,
            IFtpClientFactory ftpClientFactory) : base(config)
        {            
            this.ftpClientFactory = ftpClientFactory;            
        }
       
        protected override async Task<IEnumerable<PathInfo>> GetFilesFromSourceAsync(Uri url, CancellationToken cancellationToken = default)
        {            
            using (var ftpClient = ftpClientFactory.CreateFtpClient(RootPath))
            {
                var fileList = new List<PathInfo>();

                foreach (FtpListItem item in await ftpClient.GetListingAsync(url.LocalPath, cancellationToken))
                {
                    fileList.Add(new PathInfo
                    {
                        Name = item.Name,
                        FoldersPath = url.LocalPath.ToString(),
                        IsDirectory = item.Type == FtpFileSystemObjectType.Directory
                    });
                }

                return fileList;
            }            
        }               
        protected override async Task DownloadAndSaveFilesAsync(PathInfo pathInfo, CancellationToken cancellationToken = default)
        {
            EnsureDropFolderExists();

            using (var ftpClient = ftpClientFactory.CreateFtpClient(RootPath))
            using (var streamToWriteTo = File.Open(GetDropFullFilePath(pathInfo),
                FileMode.OpenOrCreate))
            {
                await ftpClient.DownloadAsync(streamToWriteTo, pathInfo.RelativeFilePath(),0,null, cancellationToken);
            }
        }     
    }
}
