﻿using DownloadFilesManager.Services.Configurations;
using DownloadFilesManager.Services.Helpers;
using DownloadFilesManager.Services.Protocols.Interfaces;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Linq;
using System;
using System.Runtime.CompilerServices;
using System.Threading;

[assembly: InternalsVisibleTo("DownloadFilesManager.Unit.Tests")]
namespace DownloadFilesManager.Services.Protocols.Http
{
    public class HttpProtocol : BaseProtocol, IHtmlProtocol
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly Regex fileNamesRegEx;

        public HttpProtocol(IHttpClientFactory httpClientFactory,
            IFilesConfiguration config) : base(config)
        {
            fileNamesRegEx = new Regex(config.FileNamesRegEx, RegexOptions.IgnoreCase);
            this.httpClientFactory = httpClientFactory;            
        }

        private bool IsInvalidFolder(HttpResponseMessage response) => response.StatusCode == System.Net.HttpStatusCode.NotFound;

        private async Task<string> GetHtmlWithFileList(Uri url,CancellationToken cancellationToken = default)
        {
            using (var client = httpClientFactory.CreateClient())
            {
                var response = await client.GetAsync(url, cancellationToken);

                if (IsInvalidFolder(response)) return string.Empty;

                response.EnsureSuccessStatusCode();

                return await response.Content.ReadAsStringAsync();
            }
        }

        private bool IsDirectory(string name) => string.IsNullOrEmpty(Path.GetFileName(name));
        private string GetFileOrFolderName(string path) => string.IsNullOrEmpty(Path.GetFileName(path)) ? new DirectoryInfo(path).Name : Path.GetFileName(path);        

        protected override async Task<IEnumerable<PathInfo>> GetFilesFromSourceAsync(Uri url, CancellationToken cancellationToken = default)
        {                                                            
            var pathsList = new List<PathInfo>();
 
            foreach (var item in fileNamesRegEx
                                    .Matches(await GetHtmlWithFileList(url, cancellationToken))
                                    .Where(m => m.Success))
            {
                var fileValue = item.Groups["file"].ToString();
                pathsList.Add(new PathInfo
                {
                    FoldersPath = $"{url.ToString().Replace(RootPath.TrimEnd(Path.AltDirectorySeparatorChar), string.Empty)}",
                    Name = GetFileOrFolderName(fileValue),
                    IsDirectory = IsDirectory(fileValue)
                });                
            }

            return pathsList;
        }

        protected async override Task DownloadAndSaveFilesAsync(PathInfo pathInfo, CancellationToken cancellationToken = default)
        {
            EnsureDropFolderExists();

            using var client = httpClientFactory.CreateClient();
            using (var streamToReadFrom = await client.GetStreamAsync(pathInfo.AbsoluteFilePath(RootPath)))
            using (var streamToWriteTo = File.Open(GetDropFullFilePath(pathInfo), FileMode.OpenOrCreate))
            {                                
                await streamToReadFrom.CopyToAsync(streamToWriteTo, cancellationToken);                
            }            
        }
    }
}
