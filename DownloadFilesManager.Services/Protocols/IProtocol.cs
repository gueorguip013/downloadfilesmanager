﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DownloadFilesManager.Services.Protocols
{
    public interface IProtocol
    {
        Task<int> DownloadFilesAsync(Uri url, IProgress<string> progress, CancellationToken cancellationToken = default);
    }
}
