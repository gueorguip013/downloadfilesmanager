﻿using DownloadFilesManager.Services.Helpers;
using Xunit;
using Shouldly;

namespace DownloadFilesManager.Unit.Tests.Features.Helpers
{
    public class HashHelpersTest
    {
        [Fact]
        public void GetHashTest()
        {
            var rootPath = "http://localhost/test";
            var rootPathHash = HashHelper.GetHash(rootPath);
            rootPathHash.ShouldNotBe(string.Empty);
        }

        [Fact]
        public void GetHashTestWithEmptyValue()
        {            
            var rootPathHash = HashHelper.GetHash(string.Empty);
            rootPathHash.ShouldBe(string.Empty);
        }
    }
}
