﻿using DownloadFilesManager.Services.Configurations;
using DownloadFilesManager.Services.Helpers;
using DownloadFilesManager.Services.Protocols;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DownloadFilesManager.Unit.Tests.Features.Protocols.BaseProtocolTests.BaseHelpersTest
{
    public class MockProtocol : BaseProtocol
    {

        public MockProtocol(IFilesConfiguration config) : base (config)
        {

        }
        protected override Task DownloadAndSaveFilesAsync(PathInfo pathInfo, CancellationToken cancellationToken= default)
        {
            throw new NotImplementedException();
        }

        protected override Task<IEnumerable<PathInfo>> GetFilesFromSourceAsync(Uri url, CancellationToken cancellationToken = default)
        {            
            throw new NotImplementedException();
        }
    }
}
