﻿using DownloadFilesManager.Services.Helpers;
using DownloadFilesManager.Unit.Tests.Features.Protocols.BaseProtocolTests.BaseHelpersTest;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Xunit;
using Shouldly;
using DownloadFilesManager.Services.Configurations;
using System.Threading;

namespace DownloadFilesManager.Unit.Tests.Features.Protocols.HttpTest
{    
    public class BaseProtocolTests
    {
        private readonly Uri Url = new Uri("http://localhost");
        private const string FolderPath = "test";
        private const string FolderName = "testFolder";
        private IEnumerable<PathInfo> GetPathList(int number, bool isDirectory = false) 
        {
            var pathList = new List<PathInfo>();
            for (int i = 0; i < number; i++)
            {
                pathList.Add(new PathInfo
                {
                    FoldersPath = FolderPath,
                    IsDirectory = isDirectory,
                    Name = isDirectory ? FolderName : Path.GetRandomFileName()
                });
            }

            return pathList;
        }

        [Fact]
        public async Task Process1000FilesWithConcurrentFiveTest() {
            
            var baseProtocolMock = new Mock<MockProtocol>(new FilesConfiguration { MaxDowloadFilesConcurrent = 5 });
            var testCount = 1000;
            
            baseProtocolMock.Protected()
                .Setup<Task<IEnumerable<PathInfo>>>("GetFilesFromSourceAsync", ItExpr.IsAny<Uri>(), ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(GetPathList(testCount)));

            baseProtocolMock.Protected()
                .Setup<Task>("DownloadAndSaveFilesAsync", ItExpr.IsAny<PathInfo>(), ItExpr.IsAny<CancellationToken>())
                .Returns(async () => await Task.Delay(10));                

            (await baseProtocolMock.Object.DownloadFilesAsync(Url)).ShouldBe(testCount);
        }

        [Fact]
        public async Task Process100FilesWithConcurrentOneTest()
        {
            var baseProtocolMock = new Mock<MockProtocol>(new FilesConfiguration { MaxDowloadFilesConcurrent = 1});
            var testCount = 1;

            baseProtocolMock.Protected()
                .Setup<Task<IEnumerable<PathInfo>>>("GetFilesFromSourceAsync", ItExpr.IsAny<Uri>(), ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(GetPathList(testCount)));

            baseProtocolMock.Protected()
                .Setup<Task>("DownloadAndSaveFilesAsync", ItExpr.IsAny<PathInfo>(), ItExpr.IsAny<CancellationToken>())
                .Returns(async() => await Task.Delay(10));                

            (await baseProtocolMock.Object.DownloadFilesAsync(Url)).ShouldBe(testCount);
        }

        [Fact]
        public void ThrowingExceptionGettingFiles()
        {
            var testErrorMessage = "Test";
            var baseProtocolMock = new Mock<MockProtocol>(new FilesConfiguration { MaxDowloadFilesConcurrent = 5 });
            var testCount = 100;
            
            baseProtocolMock.Protected()
                .Setup<Task<IEnumerable<PathInfo>>>("GetFilesFromSourceAsync", ItExpr.Is<Uri>(i => i.Equals(Url)), ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(GetPathList(testCount, true)));

            baseProtocolMock.Protected()
                .Setup<Task<IEnumerable<PathInfo>>>("GetFilesFromSourceAsync", ItExpr.Is<Uri>(i => i.ToString() == $"{Url}{FolderPath}/{FolderName}"), ItExpr.IsAny<CancellationToken>())
                .ThrowsAsync(new Exception(testErrorMessage));
            
            var ex = Should.Throw<Exception>(async () => await baseProtocolMock.Object.DownloadFilesAsync(Url));                
            ex.ShouldBeOfType<Exception>();
            ex.Message.ShouldBe(testErrorMessage);
        }

        [Fact]
        public void ThrowingExceptionDownloadingFiles()
        {
            var testErrorMessage = "Test";
            var baseProtocolMock = new Mock<MockProtocol>(new FilesConfiguration { MaxDowloadFilesConcurrent = 5 });
            var testCount = 100;

            baseProtocolMock.Protected()
                .Setup<Task<IEnumerable<PathInfo>>>("GetFilesFromSourceAsync", ItExpr.IsAny<Uri>(), ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(GetPathList(testCount)));

            baseProtocolMock.Protected()
                .Setup<Task>("DownloadAndSaveFilesAsync", ItExpr.IsAny<PathInfo>(), ItExpr.IsAny<CancellationToken>())
                .ThrowsAsync(new Exception(testErrorMessage));
                

            var ex = Should.Throw<Exception>(async () => await baseProtocolMock.Object.DownloadFilesAsync(Url));
            ex.ShouldBeOfType<Exception>();
            ex.Message.ShouldBe(testErrorMessage);
        }

    }
}
