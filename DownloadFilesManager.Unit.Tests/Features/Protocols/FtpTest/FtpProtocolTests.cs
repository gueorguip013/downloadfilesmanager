﻿using DownloadFilesManager.Services.Configurations;
using DownloadFilesManager.Services.Factories.FtpClientFactory;
using DownloadFilesManager.Services.Protocols.Ftp;
using FluentFTP;
using Moq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using System;
using Shouldly;
using System.Linq;

namespace DownloadFilesManager.Unit.Tests.Features.Protocols.FtpTest
{
    public class FtpProtocolTests : FtpProtocol
    {
        public FtpProtocolTests() : base(new FilesConfiguration(), GetFtpClientFactory())
        {
            
        }
        private static IFtpClientFactory GetFtpClientFactory() {

            var factory = new Mock<IFtpClientFactory>();
            var client = new Mock<IFtpClient>();
            var itemList = new FtpListItem[2];
            var item = new FtpListItem {
                Name = "Test.txt",
                Type = FtpFileSystemObjectType.File
            };

            var itemFolder = new FtpListItem
            {
                Name = "Folder",
                Type = FtpFileSystemObjectType.Directory
            };

            itemList[0] = item;
            itemList[1] = itemFolder;
            client.Setup(c => c.GetListingAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).Returns(Task.FromResult(itemList));
            factory.Setup(f => f.CreateFtpClient(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<NetworkCredential>())).Returns(client.Object);

            return factory.Object;

        }

        [Fact]
        public async void GetFilesFromSourceTest() 
        {
            var result = await GetFilesFromSourceAsync(new Uri("ftp://127.0.0.1"));
            result.ToList().Count.ShouldBe(2);
            
            result.ElementAt(0).Name.ShouldBe("Test.txt");
            result.ElementAt(0).IsDirectory.ShouldBeFalse();

            result.ElementAt(1).Name.ShouldBe("Folder");
            result.ElementAt(1).IsDirectory.ShouldBeTrue();
        }
    }
}
