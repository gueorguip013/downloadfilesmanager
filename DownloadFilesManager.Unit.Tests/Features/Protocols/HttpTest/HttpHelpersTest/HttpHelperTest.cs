﻿using Moq;
using Moq.Protected;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace DownloadFilesManager.Unit.Tests.Features.Protocols.HttpTest.HttpHelpersTest
{
    public static class HttpHelperTest
    {
        public static IHttpClientFactory GetHttpClientFactoy(string path)
        {
            var clientHandlerMock = new Mock<DelegatingHandler>();
            var httpClient = new HttpClient(clientHandlerMock.Object);
            var httpClientFactory = new Mock<IHttpClientFactory>();

            clientHandlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(File.ReadAllText(path).ToString()) });

            clientHandlerMock.As<IDisposable>().Setup(s => s.Dispose());

            httpClientFactory.Setup(cf => cf.CreateClient(It.IsAny<string>())).Returns(httpClient);

            return httpClientFactory.Object;
        }
    }
}
