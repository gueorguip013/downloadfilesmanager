﻿using DownloadFilesManager.Services.Configurations;
using DownloadFilesManager.Services.Protocols.Http;
using System;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using Shouldly;
using DownloadFilesManager.Unit.Tests.Features.Protocols.HttpTest.HttpHelpersTest;

namespace DownloadFilesManager.Unit.Tests.Features.Protocols.HttpTest
{
    public class HttpProtocolGetFilesFromIISTests : HttpProtocol
    {

        public HttpProtocolGetFilesFromIISTests() : base(HttpHelperTest.GetHttpClientFactoy("./HttpResponsesSamples/IISResponse.html"), new FilesConfiguration())
        {
        }
        
        [Fact]
        public async Task GetFilesFromIISSourceTest()
        {
            RootPath = "http://localhost";
            var result = await GetFilesFromSourceAsync(new Uri(RootPath));
            result.ToList().Count.ShouldBe(4);

        }        
    }
}
