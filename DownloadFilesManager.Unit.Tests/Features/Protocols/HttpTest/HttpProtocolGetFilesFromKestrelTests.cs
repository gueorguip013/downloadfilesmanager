﻿using DownloadFilesManager.Services.Configurations;
using DownloadFilesManager.Services.Protocols.Http;
using System;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using DownloadFilesManager.Services.Helpers;
using System.Collections.Generic;
using Shouldly;
using DownloadFilesManager.Unit.Tests.Features.Protocols.HttpTest.HttpHelpersTest;

namespace DownloadFilesManager.Unit.Tests.Features.Protocols.HttpTest
{
    public class HttpProtocolGetFilesFromKestrelTests : HttpProtocol
    {

        public HttpProtocolGetFilesFromKestrelTests() : base(HttpHelperTest.GetHttpClientFactoy("./HttpResponsesSamples/KestrelResponse.html"), new FilesConfiguration())
        {
        }
        
        [Fact]
        public async Task GetFilesFromKestrelSourceTest()
        {
            RootPath = "http://localhost";
            IEnumerable<PathInfo> result = await GetFilesFromSourceAsync(new Uri(RootPath));
            result.ToList().Count.ShouldBe(5);

        }        
    }
}
