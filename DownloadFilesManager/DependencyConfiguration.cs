﻿using DownloadFilesManager.Services;
using DownloadFilesManager.Services.Configurations;
using DownloadFilesManager.Services.Factories.FtpClientFactory;
using DownloadFilesManager.Services.Factories.ProtocolFactory;
using DownloadFilesManager.Services.Protocols.Ftp;
using DownloadFilesManager.Services.Protocols.Http;
using DownloadFilesManager.Services.Protocols.Interfaces;
using FluentFTP;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.IO;

namespace DownloadFilesManager
{
    internal static class DependencyConfiguration
    {
        private static string ConfigFilePath { get; } = Path.Combine(Directory.GetCurrentDirectory(), "Configs\\appsettings.json");
        private static readonly ServiceCollection ServiceProvider = new ServiceCollection();
        public static ServiceProvider Container;
        
        private static void BuildServiceProvider() {
            Container = ServiceProvider.BuildServiceProvider();            
        }
                
        private static void RegisterExternalServices()
        {
            ServiceProvider.AddHttpClient();
        }

        private static void RegisterConfigurationFiles()
        {
            IConfiguration config = new ConfigurationBuilder()
               .AddJsonFile(ConfigFilePath, false, false)
               .Build();

            ServiceProvider.Configure<FilesConfiguration>(config.GetSection("Files"));
            ServiceProvider.AddSingleton<IFilesConfiguration>(sp =>
                sp.GetRequiredService<IOptions<FilesConfiguration>>().Value);                        
        }

        private static void RegisterServices()
        {
            ServiceProvider.AddScoped<IHtmlProtocol, HttpProtocol>();
            ServiceProvider.AddScoped<IFtpClientFactory, FtpClientFactory>();
            ServiceProvider.AddScoped<IFtpProtocol, FtpProtocol>();
            ServiceProvider.AddScoped<IProtocolFactory>(f => new ProtocolFactory(Container));
            ServiceProvider.AddScoped<IDownloadFileManager, DownloadFileManager>();
            ServiceProvider.AddScoped<IFtpClient, FtpClient>();
        }

        public static void Configure() 
        {
            RegisterExternalServices();
            RegisterConfigurationFiles();
            RegisterServices();
            BuildServiceProvider();
        }

    }
}
