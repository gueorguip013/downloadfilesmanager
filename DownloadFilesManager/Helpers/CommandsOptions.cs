﻿using CommandLine;
using System.Collections.Generic;

namespace DownloadFilesManager.Helpers
{
    internal class CommandsOptions
    {
        [Option('f', "DropFolder", Required = false, HelpText = "Drop Folder Path.")]
        public string DropFolder { get; set; }

        [Option('p', "Paths", Required = false, HelpText = "Download Path List.")]
        public string Paths { get; set; }
    }
}
