﻿using DownloadFilesManager.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommandLine;
using DownloadFilesManager.Helpers;
using System.Linq;
using DownloadFilesManager.Services.Configurations;
using System.Runtime.CompilerServices;
using System.Threading;

[assembly: InternalsVisibleTo("DownloadFilesManager.Integration.Tests")]
namespace DownloadFilesManager
{
    public static class Program
    {
        private static IFilesConfiguration Config;
        private static readonly CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();
        
        private static IFilesConfiguration GetConfiguration()
        {
            if (Config == null)
            {
                Config = DependencyConfiguration.Container.GetService<IFilesConfiguration>();
                return Config;
            }
            else {
                return Config;
            }                
        }
        private static void OverrideDropFolder(string dropFolder)
        {
            GetConfiguration().DropFolder = dropFolder;
        }

        private static string[] GetManualPaths()
        {
            Console.Clear();
            Console.Write("Type the paths(comma separted):");
            var paths = Console.ReadLine();

            return paths.Split(',');
        }
        private static IEnumerable<string> GetCommandParams(string[] args)
        {
            IEnumerable<string> pathList = null;

            Parser.Default.ParseArguments<CommandsOptions>(args)
                .WithParsed(c =>
                {
                    if (!string.IsNullOrEmpty(c.DropFolder)) OverrideDropFolder(c.DropFolder.TrimStart());
                    if (!string.IsNullOrEmpty(c.Paths)) pathList = c.Paths.Split(',').Select(s => s.TrimStart());
                });

            return pathList != null && pathList.Count() > 0 ? pathList : null;
        }

        private static void CancelHandler(object sender, ConsoleCancelEventArgs args)
        {
            Console.WriteLine("The operation has been interrupted.");                        
            args.Cancel = true;            
            CancellationTokenSource.Cancel();            
        }

        private static void ReportProgress(string value) {
            Console.Write(value);
        }
        
        public async static Task Main(string[] args)
        {                        
            DependencyConfiguration.Configure();
            Console.CancelKeyPress += new ConsoleCancelEventHandler(CancelHandler);
            

            var pathList = GetCommandParams(args) ?? GetManualPaths();
            var downloadFileManager = DependencyConfiguration.Container.GetService<IDownloadFileManager>();

            Console.WriteLine("Starting downloading");
            Console.WriteLine("Please Wait");
            
            int totalFiles = 0;
            try
            {
                var progressIndicator = new Progress<string>(ReportProgress);
                totalFiles = await downloadFileManager.DownloadFiles(pathList, progressIndicator, CancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("\nTask cancelled");
            }
            catch (Exception)
            {
                throw;
            }
            finally {
                Console.WriteLine($"\n{totalFiles} files was downloaded succeedfully to {GetConfiguration().DropFolder}");
            }            
        }
    }
}
